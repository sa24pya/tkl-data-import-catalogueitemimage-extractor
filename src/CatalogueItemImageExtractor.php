<?php

class CatalogueItemImageExtractor
{
    private $mongoKeys = ['mongoServer','mongoPort','mongoDBName','mongoCollectionName'];
    private $logger = null;
    private $dal = null;
    private $sa24Items = [];
    /**
     * __construct() : Start this class. Also initializes the logger.
     */
    public function __construct()
    {
        $this->logger = new Logging();
        $this->logger->lfile('Error.log');
        $this->dal = DAL::getInstance();
    }
    /**
     * objectToArray(): Transforms a given object to an associative array, including its depth items
     * @param stdClass $obj : The object to transform.
     * @return array The resulting array
     */
    protected function objectToArray($obj)
    {
        if (is_object($obj)) {
            $new = (array) $obj;
            foreach ($new as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } elseif (is_array($obj)) {
            $new = (array) $obj;
            foreach ($new as $key => $val) {
                if (is_object($val)) {
                    $new[$key] = $this->objectToArray($val);
                }
            }
        } else {
            $new = $obj;
        }
        return $new;
    }
    /**
     * arrayToObject(): Transforms a given associative array to an object, including its depth items.
     * @param array $arr : The array to transform.
     * @return stdClass The resulting object
     */
    protected function arrayToObject($arr)
    {
        return json_decode(json_encode($arr), false);
    }

    public function saveToSA24($item)
    {
        $count = 0;
        $flatten = new FlattenCatalogue();
        $flatten->flatten($item);
        $items = $flatten->get_catalogueItems();
        foreach ($items as $item) {
            $this->sa24Items[] = $item;
        }
        unset($flatten);
        unset($items);
    }
    /**
     * process(): The entry point for this class
     * @param array|null $config : The config
     * @return type
     */
    public function process(array $config = null)
    {
        $filepath = 'config.json';
        if (empty($config)) {
            $config = file_get_contents($filepath);
            $config = (array) json_decode($config);
            $config = $config['development'];
        } else {
            $config = (object) $config;
        }
        $indexConfig = (array) $config->DataImport->ProductPersister;
        $mongoServer = $indexConfig['mongoServer'];
        $mongoPort = $indexConfig['mongoPort'];
        $bucket = $indexConfig['s3_bucket'];
        $cloudFrontUID = $indexConfig['cloudfront_uid'];
        $manager = new MongoDB\Driver\Manager("mongodb://$mongoServer:$mongoPort");

        // DB name
        $mongoDBName = $indexConfig['mongoDBName'];
        // Collection name
        $mongoCollectionName = $indexConfig['mongoCollectionName'];
        $mongoCollectionSA24Name = $indexConfig['mongoCollectionSA24Name'];
        $items = $this->dal->pendingImageRead($mongoCollectionName);
        $imageHandler = new CatalogueItemImageHandler($bucket, $cloudFrontUID, $this->logger);
        $idx = new IndexManipulator('Error.log');
        $bulkGS1 = new MongoDB\Driver\BulkWrite;

        // Mongo ReadPreference (Route to primary member of a replica set)
        // RP_PRIMARY is default
        $readPreference = new MongoDB\Driver\ReadPreference(
            MongoDB\Driver\ReadPreference::RP_PRIMARY
        );
        // Write concern
        $writeConcern = new MongoDB\Driver\WriteConcern(
            MongoDB\Driver\WriteConcern::MAJORITY,
            1000
        );
        $this->logger->lwrite('catalogueItemImageExtractor: Handling images...');
        if (count($items)) {
            foreach ($items as $key => $value) {
                echo("GS1_Key: $key\n");
                $arrayObject = $this->objectToArray($value);
                $gs1Item = $imageHandler->handleGS1Item($arrayObject);
                $items[$key] = $gs1Item;
                $gtin = $gs1Item['catalogueItem']['tradeItem']['gtin'];
                $arrayObject['image'] = array(
                    'pending' => false,
                    'timestamp' => time(),
                    );
                if (empty($gtin)) {
                    $message = "tradeItem-gtin is empty in: \n"
                        . var_export($gs1Item, true);
                    throw new Exception($message);
                }
                $bulkGS1->update(
                    ['catalogueItem.tradeItem.gtin' => $gtin],
                    ['$set' => $this->arrayToObject($arrayObject)],
                    ['multi' => false, 'upsert' => true]
                );
                $this->saveToSA24($this->arrayToObject($gs1Item));
            }
            try {
                echo("Saving updates...\n");
                $idx->dropIndexes($indexConfig);
                $result = $manager->executeBulkWrite("$mongoDBName.$mongoCollectionName", $bulkGS1, $writeConcern);
                $this->dal->catalogueItemSaveBulk($this->sa24Items, $mongoCollectionSA24Name);
                $idx->createIndexes($indexConfig);
                return true;
            } catch (Exception $e) {
                // write message to the log file
                $this->logger->lwrite('catalogueItemImageExtractor: '.$e.'');
                $this->logger->lclose();
                return false;
            }
        }
        $this->logger->lwrite('catalogueItemImageExtractor: No images pending found!');
        $this->logger->lclose();
        return true;
    }
}
